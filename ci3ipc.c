/* ===========================================================================
 * File: ci3ipc.c
 * Date: 12 Mar 2019
 * Creator: Brian Blumberg <blum@disroot.org>
 * Notice: Copyright (c) 2019 Brian Blumberg
 * ===========================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <i3ipc-glib/i3ipc-glib.h>

static void
floating_callback(i3ipcConnection *conn, i3ipcWindowEvent *e, gpointer data)
{
    gulong id = 0;;
    g_object_get(e->container, "id", &id, NULL);

    i3ipcCon *tree = i3ipc_connection_get_tree(conn, NULL);
    i3ipcCon *current = i3ipc_con_find_by_id(tree, id);
    i3ipcCon *parent = NULL;
    gchar *ptype;

    g_object_get(current, "parent", &parent, NULL);
    if(!parent) return;
    g_object_get(parent, "type", &ptype, NULL);
    if(!strcmp(ptype, "floating_con")) {
        /* This sleep is required as there is some race condition where the move command could be
         * called before the window is shown, thus not actually centering the window. Play with
         * this amount if the windows aren't consistently being centered. */
        g_usleep(1000);
        i3ipc_con_command(current, "move position center", NULL);
    }

    g_free(ptype);
    g_clear_object(&parent);
    g_clear_object(&current);
}

static void
shutdown_callback(i3ipcConnection *conn, gpointer data)
{
    printf("ci3ipc: Shutting down...\n");
    i3ipc_connection_main_quit(conn);
}

int
main(int argc, char **argv)
{
    GError *err = NULL;
    i3ipcConnection *conn = i3ipc_connection_new(NULL, &err);
    if(err) {
        fprintf(stderr, "ci3ipc: Unable to connect to i3: %s\n", err->message);
        exit(err->code);
    }

    GClosure *floating_closure = g_cclosure_new(G_CALLBACK(floating_callback), NULL, NULL);
    i3ipc_connection_on(conn, "window::floating", floating_closure, &err);
    if(err) {
        fprintf(stderr, "ci3ipc: Unable to subscribe to 'window' events: %s\n", err->message);
        exit(err->code);
    }

    GClosure *shutdown_closure = g_cclosure_new(G_CALLBACK(shutdown_callback), NULL, NULL);
    i3ipc_connection_on(conn, "ipc-shutdown", shutdown_closure, &err);
    if(err) {
        fprintf(stderr, "ci3ipc: Unable to subscribe to 'ipc-shutdown' events: %s\n", err->message);
        exit(err->code);
    }

    i3ipc_connection_main(conn);

    return 0;
}
